# Install Nanopore MinION

## Buy a computer

由于经费有限，反复推敲后放弃了laptop的想法，最终实验室买了一台Dell 7060外加 1T 
ssd。参考[官方推荐配置清单](https://community.nanoporetech.com/requirements_documents/minion-it-reqs.pdf)，主要配置如下

- intel i8700 6c/12t
- 32G DDR4
- 1T samsung 860 evo SSD pcie
- 1T WD blue

## Install Ubuntu 16.04 LTS

目前 MinKnow 官方只支持 Ubuntu 16.04 LTS版本，对于 18.04 的用户就要自己想办法解
决了。具体过程不详述了，2块硬盘 HD 是 /dev/sda，SSD 是 /dev/sdb，也没有进一步分区，
SSD 安装系统，分了一个/efi 512M，swap用swapfile解决，其余空间划分成一个分区全部挂
载到 / 上。sda 也只划分成一个分区挂载到 /data 上。

## Install Minknow on Ubuntu

```bash
$ sudo apt-get update
$ wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
$ echo "deb http://mirror.oxfordnanoportal.com/apt xenial-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
$ sudo apt-get update 
$ sudo apt-get install minknow-nc
```

完毕后，以下软件就已经自动安装到系统中。

- MinKNOW
- Albacore
- Guppy
- Dogfish
- EPI2ME

MinKNow 是测序控制软件，Albacore 是官方的 basecaller，过去的 basecaller 使用贝叶斯等算法，
Albacore 使用了 RNN 神经网络算法，获得的碱基序列更为准确。Guppy 是基于 GPU的 basecaller，
Dogfish 用于台式测序仪，EPI2ME 是一个官方的后期分析软件。

## Run MinKNOW GUI

```bash
$ /opt/ui/Minknow
```

