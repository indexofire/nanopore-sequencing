# Platform QC

参考[文档](https://community.nanoporetech.com/protocols/experiment-companion-platform-qc/v/pqe_1004_v1_revu_06jan2016/platform-qc)
进行芯片质控。

- 需要在收到芯片5天内检测纳米孔活性和数量。
- 芯片不使用时，应保存在 2~8 摄氏度。